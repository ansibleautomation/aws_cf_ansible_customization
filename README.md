**Ansible deployment using Cloudformation Templates **


* `cfvolcreate1.yml ` > - Standard Provisioning Using CF parameters. Creates single volume using the CF template in yml  

* `cfvolcreate2.yml ` > - Multi Volume Provisioning using Ansible loop structure  

*  `cfvolcreate3.yml ` > - Multi Volume with variable size(s) Provisioning using Ansible Nested loops.  


Usage:

> ansible-playbook -i "localhost," -c local -e "count=2" -e '{"disksize": [1,2]}' -e '{"splitsize": [1,1]}' cfvolcreate3.yml 

> ansible-playbook -i "localhost," -c local -e "count=2" -e "volsize=4" cfvolcreate2.yml 

> ansible-playbook -i "localhost," -c local cfvolcreate1.yml 